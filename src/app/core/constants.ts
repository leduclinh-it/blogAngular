export const FrontUrl = {
  HOME: 'home',
  ABOUT: 'about',
  LOGIN_PAGE: 'login',
  REGISTER_PAGE: 'register',
  CONTACT: 'contact',
  POSTS: 'posts',
  POST_DETAIL: 'post-detail',
};

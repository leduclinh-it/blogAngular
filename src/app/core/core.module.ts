import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// @ts-ignore
import { CoreRoutingModule } from './core/core-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CoreRoutingModule
  ]
})
export class CoreModule { }

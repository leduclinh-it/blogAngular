import { Injectable } from '@angular/core';
import * as firebase from "firebase";
import { AngularFireAuth } from '@angular/fire/auth';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private afAuth: AngularFireAuth) { }

  loginWithFacebook() {
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.FacebookAuthProvider();
      this.afAuth.idToken.subscribe(value => console.log(value))
      this.afAuth
        .signInWithPopup(provider)
        .then(res => {
          console.log(res);
        }, err => {
          console.log(err);
          reject(err);
        })
    })

  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import * as CONST from '../core/constants';
import {UserLayoutComponent} from './main-layouts/user-layout/user-layout.component';
import {HomePageComponent} from './components/home-page/home-page.component';
import {AboutPageComponent} from './components/about-page/about-page.component';
import {PostPageComponent} from './components/post-page/post-page.component';
import {ContactPageComponent} from './components/contact-page/contact-page.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {PostDetailComponent} from './components/post-page/post-detail/post-detail.component';

const routes: Routes = [
  {path: '', redirectTo: CONST.FrontUrl.ABOUT, pathMatch: 'full'},
  {path: '', component: UserLayoutComponent,
    children: [
      {path: CONST.FrontUrl.HOME, component: HomePageComponent},
      {path: CONST.FrontUrl.ABOUT, component: AboutPageComponent},
      {path: CONST.FrontUrl.POSTS, component: PostPageComponent},
      {path: CONST.FrontUrl.CONTACT, component: ContactPageComponent},
      {path: CONST.FrontUrl.LOGIN_PAGE, component: LoginComponent},
      {path: CONST.FrontUrl.REGISTER_PAGE, component: RegisterComponent},
      {path: CONST.FrontUrl.POST_DETAIL, component: PostDetailComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeatureRoutingModule { }

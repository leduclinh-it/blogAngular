import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatureRoutingModule } from './feature-routing.module';
import { UserLayoutComponent } from './main-layouts/user-layout/user-layout.component';
import { AdminLayoutComponent } from './main-layouts/admin-layout/admin-layout.component';
import { HeaderComponent } from './main-layouts/user-layout/header/header.component';
import { FooterComponent } from './main-layouts/user-layout/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { PostPageComponent } from './components/post-page/post-page.component';
import { AboutPageComponent } from './components/about-page/about-page.component';
import { ContactPageComponent } from './components/contact-page/contact-page.component';
import { PostDetailComponent } from './components/post-page/post-detail/post-detail.component';
import {MDBBootstrapModule} from 'angular-bootstrap-md';


@NgModule({
  declarations: [
    UserLayoutComponent,
    AdminLayoutComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    HomePageComponent,
    PostPageComponent,
    AboutPageComponent,
    ContactPageComponent,
    PostDetailComponent,
  ],
  imports: [
    CommonModule,
    FeatureRoutingModule,
    MDBBootstrapModule.forRoot(),


  ]
})
export class FeatureModule { }

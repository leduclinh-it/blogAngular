// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyDNea949l7mgeZek8gIbXN-F7yfUAT7PrM',
    authDomain: 'blogangular-cb26f.firebaseapp.com',
    databaseURL: 'https://blogangular-cb26f.firebaseio.com',
    projectId: 'blogangular-cb26f',
    storageBucket: 'blogangular-cb26f.appspot.com',
    messagingSenderId: '229543248797',
    appId: '1:229543248797:web:dbc295f8b1d2582bb32574',
    measurementId: 'G-1989TF2LQ3'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
